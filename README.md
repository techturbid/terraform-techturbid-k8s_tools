Module to provision a minimal GKE for Techturbid needs


You will need the following vars in your workspace to make it work:
```
cluster_env="temp"
cluster_name="acdc"
cluster_region="us-central1"
cluster_zone="us-central1-a"
service-account_email=""
gcp_project="techtur-bid-acdc-temp"
preemptible_nodes="true"
machine_type="n1-standard-1"
node_count="3"
nodes_disk_size="20"
```
