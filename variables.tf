variable "gcp_project" {
  description = "GPC project the cluster will be deployed"
}

variable "service_account_key" {
  description = "GCP service-account JSON key"
}

variable "cluster_name" {
  description = "Cluster name"
}

variable "cluster_version" {
  default     = "latest"
  description = "Set an specific gke version or leave blank to use the latest"
}

variable "gcp_region" {
  default     = "us-central1"
  description = "Cluster region"
}

variable "gcp_zone" {
  default     = "us-central1-a"
  description = "Cluster zone"
}

variable "regional_cluster" {
  default = "false"
  description = "(true/false) Whether to create a regional cluster or not"
}

variable "preemptible_nodes" {
  default     = "true"
  description = "(true/false) Wether to use preemptible nodes or not in this workspace"
}

variable "node_count" {
  default     = "3"
  description = "Number of nodes to deployed on each zone"
}

variable "machine_type" {
  default     = "n1-standard-2"
  description = "GCE machine type to be used by the nodes"
}

variable "nodes_disk_size" {
  default = "50"
  description = "Disk size for each node"
}

variable "cloud_dns_project" {
  default = "techtur-bid-commons"
  description = "GCP project used by Cloud DNS to manage the zone "
}

variable "cloud_dns_zone-name" {
  default     = "techtur-bid"
  description = "GCP project used by Cloud DNS to manage the zone "
}

variable "daily_maintenance_window_start_time" {
    default = "01:00"
  description = <<EOF
The start time of the 4 hour window for daily maintenance operations RFC3339
format HH:MM, where HH : [00-23] and MM : [00-59] GMT.
EOF
}

variable "http_load_balancing_disabled" {
  default = "false"
  description = <<EOF
The status of the HTTP (L7) load balancing controller addon, which makes it
easy to set up HTTP load balancers for services in a cluster. It is enabled
by default; set disabled = true to disable.
EOF
}

variable "gke_version" {
  default = "latest"
  description = "Which gke version to deploy"
}

variable "tiller_version" {
  default = "2.14.3"
  description = "Tiller version"
}

variable "istio_version" {
  default = "1.3.3"
  description = "Istio version"
}

variable "admin_user" {
  description = "admin user for kubernetes secretes"
}
variable "admin_password" {
  description = "admin password for kubernetes secrets"
}
