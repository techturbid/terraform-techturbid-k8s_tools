provider "google" {
  credentials = var.service_account_key
  project     = var.gcp_project
  region      = var.gcp_region
}

provider "google-beta" {
  credentials = var.service_account_key
  project = var.gcp_project
  region  = var.gcp_region

}

data "google_dns_managed_zone" "techturbid" {
  name    = var.cloud_dns_zone-name
  project = var.cloud_dns_project
}

data "google_client_config" "google_current" {
  provider = "google"
}

data "google_client_config" "google-beta_current" {
  provider = "google-beta"
}

provider "kubernetes" {
  load_config_file = false
  host = "https://${module.gke.cluster_endpoint}"
  cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
  token = data.google_client_config.google-beta_current.access_token
}

provider "helm" {
  install_tiller  = true
  tiller_image    = "gcr.io/kubernetes-helm/tiller:v${var.tiller_version}"
  service_account = module.tiller.tiller_rolebinding_name
  namespace       = module.tiller.tiller_rolebinding_namespace

  kubernetes {
    load_config_file = false
    host = "https://${module.gke.cluster_endpoint}"
    cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
    token = data.google_client_config.google-beta_current.access_token
  }
}

data "helm_repository" "jetstack" {
  name = "jetstack"
  url  = "https://charts.jetstack.io"
}