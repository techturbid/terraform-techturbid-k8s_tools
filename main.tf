module "gke-vpc" {
  source             = "app.terraform.io/techturbid/gke_vpc/google"
  version            = ">= 0.2.1"
  CLUSTER_NAME       = var.cluster_name
  wait_for_resources = [var.cluster_name]
}
module "gke" {
  source  = "app.terraform.io/techturbid/gke/google"
  version = ">= 0.3.1"

  wait_for_resources            = [module.gke-vpc.vpc_network_name]
  CLUSTER_LOCATION              = var.gcp_zone
  CLUSTER_NAME                  = var.cluster_name
  CLUSTER_VERSION               = var.cluster_version
  GCP_PROJECT                   = var.gcp_project
  MACHINE_TYPE                  = var.machine_type
  NODES_DISK_SIZE               = var.nodes_disk_size
  NODE_COUNT                    = var.node_count
  PREEMPTIBLE_NODES             = var.preemptible_nodes
  REGIONAL_CLUSTER              = var.regional_cluster
  cluster_secondary_range_name  = module.gke-vpc.cluster_secondary_range_name
  services_secondary_range_name = module.gke-vpc.services_secondary_range_name
  vpc_network_name              = module.gke-vpc.vpc_network_name
  vpc_subnetwork_name           = module.gke-vpc.vpc_subnetwork_name
  http_load_balancing_disabled  = var.http_load_balancing_disabled
  GCP_SA_PROJECT = "techtur-bid-commons"
}
module "tiller" {
  source             = "app.terraform.io/techturbid/tiller/kubernetes"
  version            = ">= 0.2.1"
  wait_for_resources = [module.gke.node_pool_id]
}

module "istio" {
  source  = "app.terraform.io/techturbid/istio/helm"
  version = ">= 0.3.1"

  CLUSTER_NAME       = var.cluster_name
  DNS_MANAGED_ZONE   = var.cloud_dns_zone-name
  GRAFANA_PASSWORD   = var.admin_password
  GRAFANA_USER       = var.admin_user
  KIALI_PASSWORD     = var.admin_password
  KIALI_USER         = var.admin_user
  wait_for_resources = [module.tiller.tiller_rolebinding_name]
  techturbidDOMAIN_NAME = "techtur.bid"
  GCP_DNS_PROJECT = "techtur-bid-commons"
  istio_version = "1.3.3"
}

module "istio_dns" {
  source  = "app.terraform.io/techturbid/istio_dns/google"
  version = ">= 0.2.0"

  CLUSTER_NAME       = var.cluster_name
  wait_for_resources = [module.istio.istio_id]
}

module "cert_manager" {
  source              = "app.terraform.io/techturbid/cert_manager/helm"
  version             = ">= 0.3.7"
  CLOUDDNS_PROJECT    = var.cloud_dns_project
  SERVICE-ACCOUNT_KEY = var.service_account_key
  wait_for_resources  = [module.istio_dns.dns_wildcard_name]
  CLUSTER_NAME        = var.cluster_name
}

module "jenkins" {
  source  = "app.terraform.io/techturbid/jenkins/helm"
  version = ">= 0.1.0"
  CLUSTER_NAME = var.cluster_name
  wait_for_resources = [module.cert_manager.cert-manager-ca]
}